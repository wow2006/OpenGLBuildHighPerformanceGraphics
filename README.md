# OpenGL - Build High Performance Graphics [![Build Status](https://travis-ci.org/wow2006/OpenGLBuildHighPerformanceGraphics.svg?branch=master)](https://travis-ci.org/wow2006/OpenGLBuildHighPerformanceGraphics)
Port OpenGL Build High Performance Graphics to CMake. ([Book link](https://www.amazon.com/OpenGL-Build-high-performance-graphics-ebook/dp/B07124RCBT/ref=sr_1_1?ie=UTF8&qid=1533807476&sr=8-1&keywords=OpenGL+Build+High+Performance+Graphics), [Original Source code](https://github.com/PacktPublishing/OpenGL-Build-High-Performance-Graphics))

This book split to three modules
- [Module 1](Module1/) : OpenGL Development Cookbook `WIP`
- [Module 2](Module2/) : OpenGL 4.0 Shading Language Cookbook `TODO`
- [Module 3](Module3/) : OpenGL Data Visualization Cookbook `TODO`

### Dependies:
- OpenGL
- GLUT `It will be replaced with SDL2`
- GLEW `It will be replaced with gl3d`
- GLM   
- SOIL `It will be replaced with stb`

Installation:
-------------

#### Ubuntu 16.04
```sh
$ sudo apt install freeglut3-dev libglew-dev libglm-dev libsoil-dev
```
#### Arch
```sh
$ sudo pacman -S freeglut glew glm soil
```

### License
MIT
